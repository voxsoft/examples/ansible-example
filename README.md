# Ansible playground
1. Get your ssh key in place
2. Install Ansible `pip install ansible-core`
3. Explicitly set the Ansible config file path if you are running on WSL
    ```sh
    export ANSIBLE_CONFIG=./ansible.cfg
    ```
4. Run the module ping
    ```sh
    ansible -m ping all
    ```
5. Run the playbook
    ```sh
    ansible-playbook ./playbooks/host.yaml
    ```

## Apply playbook to the localhost
In case there is no ssh access to the remote host, you can clone this repo and run the playbook locally
```sh
apt install python3-pip
pip install ansible-core
export PATH=/home/$USER/.local/bin:$PATH
ansible-galaxy collection install community.general
ansible-playbook -c local -i localhost, ./playbooks/new-host-init.yaml
```

## Ansible Vault
Ansible Vault a bit cumbersome but if you still want to use it here are some examples.  
> NOTE: Consider using [HashiCorp Vault](https://www.vaultproject.io/) instead to store secrets and credentials.  

#### Encrypt entire file in place
```sh
ansible-vault encrypt files/example.keystore
```
#### Encrypt specific string in variables file
It is not possible to encrypt a specific value in file in place 
so you need to encrypt the value using `ansible-vault encrypt_string` command, and paste it back in the file
```sh
## Option1: Provide input interactively and Press Ctrl+D twice to finish
ansible-vault encrypt_string --stdin-name 'keystore_password'
## Option2: Pass content from a file
cat .local/example.keystore.password | ansible-vault encrypt_string --stdin-name 'keystore_password'
```

#### Decrypt entire file in place
```sh
ansible-vault decrypt files/example.keystore
```
#### Show encrypted file content
```sh
ansible-vault view files/example.keystore
```
#### Show encrypted values in file
It is not possible to view a specific value in file using `ansible-vault` command.
Here is a several options to view encrypted values in yaml file:
1. Use yq to get the encrypted value from the file and decrypt it using `ansible-vault decrypt` command
    ```sh
    yq -r ".keystore_password" ./inventories/host_vars/localhost.yaml | ansible-vault decrypt
    ```
    - Pros: Works for any file/string size
    - Cons: Requires additional installation of `yq`
2. Use `vdecr` function from [./scripts/vdecr.sh](./scripts/vdecr.sh) 
    script to view the encrypted values in file  
    ```sh
    ## Source the script
    source ./scripts/vdecr.sh
    ## Use vdecr function to view encrypted values in file
    vdecr ./inventories/host_vars/localhost.yaml
    ```
    - Pros: No additional requirements
    - Cons: Works only for small strings; Parsing issues with multiline strings

## Roles
Check other requirements for each role in the corresponding README.md file
- [ntp](./roles/ntp/README.md) - Configure NTP using systemd-timesyncd, require `community.general` collection
- [host-init](./roles/host-init/README.md) - Install basic packages, upgrade and configure the host
- [ad-join](./roles/ad-join/README.md) - Join a host to an Active Directory domain
- [docker](./roles/docker/README.md) - Install Docker packages

## Dynamic GCE inventory
1. Install required Python packages:
    ```sh
    pip install google-auth requests
    ```
2. Install **google.cloud** collection (this will include google.cloud.gcp_compute inventory plugin):
    ```sh
    ansible-galaxy collection install google.cloud
    ```
3. Authenticate to GCP:
    ```sh
    gcloud auth application-default login
    ```
4. List GCE instances:
    ```sh
    ansible-inventory -i hosts-de-gcp.yaml --graph
    ```
5. Run the playbook against GCE instances from the dynamic inventory:
    ```sh
    ansible-playbook -i hosts-de-gcp.yaml ./playbooks/host.yaml
    ```
