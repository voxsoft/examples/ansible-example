#!/bin/sh
# Many thanks to the author: https://github.com/kolbasky/dotfiles/blob/master/.bashrc.d/aliases#L28
echo "You can use this script to decrypt vault values from Ansible vault files."
echo "Usage:"
echo "  You need to source this script and then use vdecr function."
echo "  If no file is specified as argument, function will read from STDIN."
echo "  source vdecr.sh"
echo "  vdecr /path/to/file.yml"

vdecr() {
  unset IFS
  # if we have an argument, check wether it is a file.
  if [[ -n "$1" ]]; then
    if [[ -f "$1" ]]; then
      # if argument is a file check if it is fully encrypted file and cat it to decrypt
      if [[ $(head -n1 "$1") == "\$ANSIBLE_VAULT;1.1;AES256" ]]; then
        cat "$1" | ansible-vault decrypt 2> /dev/null
        return 0
      fi
      # if argument is a file, but not fully encrypted, grep for vault values in it
      # grep uses -- as separator, and IFS is one symbol long, using - as IFS would break some variables
      # replace -- with #
      # if vault vars go one after another without empty line, grep won't print separator.
      # so add # in between
      printf 'Reading vault values from file...\n\n'
      IFS="#"
      ## TODO: read the lines until there are lines begins with numbers instead of using -A6
      for v in `grep "\!vault |" -A6 "$1" | sed 's/^--$/#/g' | sed -r 's/^(.*:)/#\n\1/g'`;do 
        if [[ "$v" != "" ]]; then 
          # print var name and pass string to vdecr. xargs trims spaces.
          printf "$v" | grep -o "^.*: " | xargs && printf '  ' && vdecr "$v"; 
        fi ; 
      done
      unset IFS
      return 0
    fi
    # if argument is not a file, just set variable
    local str="$1"
  # if STDIN is not terminal, read from STDIN
  elif [[ ! -t 0 ]]; then
    local str=$(cat)
  # if STDIN is a terminal, print message and read from STDIN
  else
    printf 'Interactive mode. Paste encrypted string and press Ctrl-D two times to confirm.\n'
    local str=$(cat)
    printf '\n'
  fi
  # decrypt var.
  # newlines may get squashed, so replace all spaces with newlines
  # remove ---, var name, !vault, | and empty lines
  printf -- "$str" | sed 's/ /\n/g' | \
  sed '/---\|^.*:\|\!vault\||\|^$/d' | \
  ansible-vault decrypt 2> /dev/null | \
  tail -n1
  printf '\n'
  unset str
  unset v
}
